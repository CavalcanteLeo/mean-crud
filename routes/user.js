var mongoose = require('mongoose');
var crypto = require('crypto');
mongoose.connect('mongodb://localhost/mean-crud');

var Schema = mongoose.Schema;

function encodePassword(password) {
    return crypto.createHash('sha1').update(password).digest('hex');
}
var usersSchema = new Schema({
	name: {type: String, required: true},
	email: {type: String, required: true, index: {unique: true}},
	password: {type: String, required: true, set: encodePassword},
	creation_date: {type: Date, default: Date.now}
});

var User = mongoose.model('users', usersSchema);

exports.list = function(req, res){
	User.find({}, null, {skip: req.params.from, limit: req.params.to}, function(err, result){
		if(err) res.json(400, {message:err});
		else res.json(200, {users:result});
	});
};

exports.create = function(req, res){
	var user = new User(req.body);
	user.save(function(err, result){
		if(err) res.json(400, {message:err.message});
		else res.json(200, {users:result});
	});
};

exports.update = function(req, res){
	console.log(req.params.id, req.body);
	User.findOneAndUpdate({_id:req.params.id}, {$set: req.body}, function(err, result){
		if(result == null) res.json(400, {message:"User not found!"});
		else res.json(200, {message:"User Updated!", user_id:result._id});
	});
};

exports.delete = function(req, res){
	User.findOneAndRemove({_id:req.params.id}, function(err, result){
		if(result == null) res.json(400, {message:"User not found!"});
		else res.json(200, {message:"User Deleted!", user_id:result._id});
	});	
};